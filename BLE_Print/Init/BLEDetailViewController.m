//
//  BLEDetailViewController.m
//  Init
//
//  Created by mc on 16/11/2.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "BLEDetailViewController.h"
#import "SVProgressHUD.h"
#import "UIImage+Bitmap.h"
#import "HLPrinter.h"
#import "HLBLEManager.h"

@interface BLEDetailViewController ()

@end

@implementation BLEDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // 初始化数据
    [self _initDataList] ;
    // 创建试图
    [self _initViews] ;
    
    // 配置蓝牙连接
    [self loadBLEInfo] ;
}

#pragma mark - 初始化数据 --------------------------------------------------------
// 初始化数据
- (void)_initDataList
{
    self.infos = [NSMutableArray array] ;
    self.titleArray = @[@"存酒",@"取酒",@"消费明细单",@"会员充值小票"] ;
}

#pragma mark - 创建试图 -----------------------------------------------------
// 创建试图
- (void)_initViews
{
    self.tableView.delegate = self ;
    self.tableView.dataSource = self ;
}
#pragma mark - 请求数据 -----------------------------------------------------
// 数据请求
- (void)_requestDataList
{
    
}


- (void)loadBLEInfo
{
    HLBLEManager *manager = [HLBLEManager sharedInstance] ;
    [manager connectPeripheral:self.perpheral
                connectOptions:@{CBConnectPeripheralOptionNotifyOnDisconnectionKey:@(YES)}
        stopScanAfterConnected:YES
               servicesOptions:nil
        characteristicsOptions:nil
                 completeBlock:^(HLOptionStage stage, CBPeripheral *peripheral, CBService *service, CBCharacteristic *character, NSError *error) {
                     switch (stage) {
                         case HLOptionStageConnection:
                         {
                             if (error) {
                                 [SVProgressHUD showErrorWithStatus:@"连接失败"];
                                 
                             } else {
                                 [SVProgressHUD showSuccessWithStatus:@"连接成功"];
                                 
                             }
                             break;
                         }
                         case HLOptionStageSeekServices:
                         {
                             if (error) {
                                 [SVProgressHUD showSuccessWithStatus:@"查找服务失败"];
                             } else {
                                 [SVProgressHUD showSuccessWithStatus:@"查找服务成功"];
                                 [_infos addObjectsFromArray:peripheral.services];
//                                 [_tableView reloadData];
                             }
                             break;
                         }
                         case HLOptionStageSeekCharacteristics:
                         {
                             // 该block会返回多次，每一个服务返回一次
                             if (error) {
                                 NSLog(@"查找特性失败");
                             } else {
                                 NSLog(@"查找特性成功");
//                                 [_tableView reloadData];
                             }
                             break;
                         }
                         case HLOptionStageSeekdescriptors:
                         {
                             // 该block会返回多次，每一个特性返回一次
                             if (error) {
                                 NSLog(@"查找特性的描述失败");
                             } else {
                                 //                                 NSLog(@"查找特性的描述成功");
                             }
                             break;
                         }
                         default:
                             break;
                     }
    }] ;
}


// 打印小票

- (IBAction)printClick:(id)sender {
    NSDictionary *dict1 = @{@"name":@"铅笔",@"amount":@"5",@"price":@"2.0"};
    NSDictionary *dict2 = @{@"name":@"橡皮",@"amount":@"1",@"price":@"1.0"};
    NSDictionary *dict3 = @{@"name":@"笔记本",@"amount":@"3",@"price":@"3.0"};
    NSDictionary *dict4 = @{@"name":@"套餐",@"amount":@"10",@"price":@"7.0"};
    self.goodsArray = @[dict1, dict2,dict3, dict4];
    
    
    for (CBService *service in self.infos) {
        for (CBCharacteristic *character in service.characteristics) {
            CBCharacteristicProperties properties = character.properties;
            if (properties & CBCharacteristicPropertyWrite) {
                //        if (self.chatacter == nil) {
                //            self.chatacter = character;
                //        }
                self.chatacter = character;
            }
        }
    }
}


- (HLPrinter *)getPrinter
{
    return  [self storageWine] ;
//    HLPrinter *printer = [[HLPrinter alloc] init];
//    NSString *title = @"皇后酒吧";
//    NSString *str1 = @"精洲华娱";
//    [printer appendText:title alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleBig];
//    [printer appendText:str1 alignment:HLTextAlignmentCenter];
//    // 条形码
//    //    [printer appendBarCodeWithInfo:@"123456789012"];
//    [printer appendSeperatorLine];
//
//    [printer appendTitle:@"时间:" value:@"2016-10-29 10:01:50" valueOffset:150];
//    [printer appendTitle:@"订单:" value:@"4000020160427100150" valueOffset:150];
//    [printer appendText:@"地址:杭州市西湖区天目山路224号" alignment:HLTextAlignmentLeft];
//
//    [printer appendSeperatorLine];
//    [printer appendLeftText:@"商品" middleText:@"数量" rightText:@"单价" isTitle:YES];
//    CGFloat total = 0.0;
//    for (NSDictionary *dict in self.goodsArray) {
//        [printer appendLeftText:dict[@"name"] middleText:dict[@"amount"] rightText:dict[@"price"] isTitle:NO];
//        total += [dict[@"price"] floatValue] * [dict[@"amount"] intValue];
//    }
//
//    [printer appendSeperatorLine];
//    NSString *totalStr = [NSString stringWithFormat:@"%.2f",total];
//    [printer appendTitle:@"总计:" value:totalStr];
//    [printer appendTitle:@"实收:" value:@"100.00"];
//    NSString *leftStr = [NSString stringWithFormat:@"%.2f",100.00 - total];
//    [printer appendTitle:@"找零:" value:leftStr];
//
//    [printer appendSeperatorLine];
//    // 二维码
//    [printer appendText:@"位图方式打印二维码" alignment:HLTextAlignmentCenter];
//    [printer appendQRCodeWithInfo:@"www.baidu.com"];
//    [printer appendSeperatorLine];
//
//    [printer appendText:@"指令方式打印二维码" alignment:HLTextAlignmentCenter];
//    [printer appendQRCodeWithInfo:@"www.baidu.com" size:12];
//    [printer appendSeperatorLine];
//
//    // 图片
//    [printer appendImage:[UIImage imageNamed:@"ico180"] alignment:HLTextAlignmentCenter maxWidth:300];
//    [printer appendFooter:nil];
//
//    for (int i = 0 ; i<7; i++) {
//        [printer appendNewLine] ;
//    }
//    // 切纸
//    [printer appendCutOff] ;
//    return printer ;
}




#pragma mark - 方法 --------------------------------------------------------
#pragma mark - 代理方法 --------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.titleArray.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"cellID" ;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID] ;
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID] ;
    }
    cell.textLabel.text = self.titleArray[indexPath.row] ;
    return cell ;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HLPrinter *printInfo = nil;
    
    switch (indexPath.row) {
        case 0:
            // 打印存酒小票
            printInfo = [self storageWine] ;
            break;
        case 1:
            // 取酒
            printInfo = [self takeWin] ;
            break;
        case 2:
            // 消费明细单
            printInfo = [self consumption] ;
            break;
        case 3:
            // 会员充值
            printInfo = [self vipRecharge] ;
        default:
            break;
    }
    NSData *mainData = [printInfo getFinalData];
    HLBLEManager *bleManager = [HLBLEManager sharedInstance];
    if (self.chatacter.properties & CBCharacteristicPropertyWrite) {
        [bleManager writeValue:mainData forCharacteristic:self.chatacter type:CBCharacteristicWriteWithResponse completionBlock:^(CBCharacteristic *characteristic, NSError *error) {
            if (!error) {
                NSLog(@"写入成功");
            }
        }];
    } else if (self.chatacter.properties & CBCharacteristicPropertyWriteWithoutResponse) {
        [bleManager writeValue:mainData forCharacteristic:self.chatacter type:CBCharacteristicWriteWithoutResponse];
    }
    
}



#pragma mark -  存酒
- (HLPrinter *)storageWine
{
    
    int count = 1 ;
    HLPrinter *printer = [[HLPrinter alloc] init];
    for (int i = 0 ; i<count; i++)
    {
        NSString *title = @"皇后酒吧";
        [printer appendText:title alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleBig];
        NSString *title2 = @"存酒";
        [printer appendText:title2 alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleMiddle];
        NSString *str1 = nil ;
        if(i==0)
        {
            str1 = [NSString stringWithFormat:@"［ 主 联 单 ］"] ;
        }
        else
        {
            str1 = [NSString stringWithFormat:@"［ 副 联 单 ］%d",i] ;
        }
        [printer appendText:str1 alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleSmalle];
        [printer appendSeperatorLineWithLeft];
        NSString *startString = @"2016-10-29" ;
        [printer appendText:[NSString stringWithFormat:@"存酒日期: %@",startString] alignment:HLTextAlignmentLeft] ;
        NSString *endString = @"2016-11-05" ;
        [printer appendText:[NSString stringWithFormat:@"过期日期: %@",endString] alignment:HLTextAlignmentLeft] ;
        NSString *string1 = @"0000309" ;
        [printer appendText:[NSString stringWithFormat:@"存酒单号: %@",string1] alignment:HLTextAlignmentLeft] ;
        NSString *string2 = @"100001" ;
        [printer appendText:[NSString stringWithFormat:@"会员卡号: %@",string2] alignment:HLTextAlignmentLeft] ;
        [printer appendSeperatorLineWithLeft] ;
        NSString *string3 = @"柠檬汁" ;
        [printer appendText:[NSString stringWithFormat:@"酒品名称: %@",string3] alignment:HLTextAlignmentLeft] ;
        NSString *string4 = @"12" ;
        [printer appendText:[NSString stringWithFormat:@"数    量: %@",string4] alignment:HLTextAlignmentLeft] ;
        NSString *string5 = @"50" ;
        [printer appendText:[NSString stringWithFormat:@"余    量: %@ (％或mL)",string5] alignment:HLTextAlignmentLeft] ;
        NSString *string6 = @"A10" ;
        [printer appendText:[NSString stringWithFormat:@"房台名称: %@",string6] alignment:HLTextAlignmentLeft] ;
        NSString *string7 = @"叶超" ;
        [printer appendText:[NSString stringWithFormat:@"服 务 员: %@",string7] alignment:HLTextAlignmentLeft] ;
        NSString *string8 = @"王老五" ;
        [printer appendText:[NSString stringWithFormat:@"客户姓名: %@",string8] alignment:HLTextAlignmentLeft] ;
        NSString *string9 = @"18511144335" ;
        [printer appendText:[NSString stringWithFormat:@"手机号码: %@",string9] alignment:HLTextAlignmentLeft] ;
        NSString *string10 = @"出品员" ;
        [printer appendText:[NSString stringWithFormat:@"操 作 员: %@",string10] alignment:HLTextAlignmentLeft] ;
        [printer appendSeperatorLineWithLeft] ;
        [printer appendNewLine] ;
        NSString *string11 = @"________________" ;
        [printer appendText:[NSString stringWithFormat:@"管理员签字: %@",string11] alignment:HLTextAlignmentLeft] ;
        for (int i = 0 ; i<6; i++) {
            [printer appendNewLine] ;
        }
        // 切纸
        [printer appendCutOff] ;
    }
    return printer;
}

#pragma mark -  取酒
- (HLPrinter *)takeWin
{
    
    int count = 1 ;
    HLPrinter *printer = [[HLPrinter alloc] init];
    for (int i = 0 ; i<count; i++) {
        NSString *title = @"皇后酒吧";
        [printer appendText:title alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleBig];
        NSString *title2 = @"取酒";
        [printer appendText:title2 alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleMiddle];
        NSString *str1 = nil ;
        if(i==0)
        {
            str1 = [NSString stringWithFormat:@"［ 主 联 单 ］"] ;
        }
        else
        {
            str1 = [NSString stringWithFormat:@"［ 副 联 单 ］%d",i] ;
        }
        [printer appendText:str1 alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleSmalle];
        [printer appendSeperatorLineWithLeft];
        NSString *startString = @"1238123" ;
        [printer appendText:[NSString stringWithFormat:@"取酒单号: %@",startString] alignment:HLTextAlignmentLeft] ;
        NSString *endString = @"100001" ;
        [printer appendText:[NSString stringWithFormat:@"会员卡号: %@",endString] alignment:HLTextAlignmentLeft] ;
        NSString *string1 = @"A10" ;
        [printer appendText:[NSString stringWithFormat:@"房台名称: %@",string1] alignment:HLTextAlignmentLeft] ;
        [printer appendSeperatorLineWithLeft] ;
        NSString *string2 = @"12" ;
        [printer appendText:[NSString stringWithFormat:@"数    量: %@",string2] alignment:HLTextAlignmentLeft] ;
        NSString *string3 = @"50" ;
        [printer appendText:[NSString stringWithFormat:@"余    量: %@ (％或mL)",string3] alignment:HLTextAlignmentLeft] ;
        NSString *string4 = @"2016-10-29" ;
        [printer appendText:[NSString stringWithFormat:@"存酒日期: %@",string4] alignment:HLTextAlignmentLeft] ;
        NSString *string5 = @"18511144335" ;
        [printer appendText:[NSString stringWithFormat:@"手机号码: %@",string5] alignment:HLTextAlignmentLeft] ;
        NSString *string6 = @"叶超" ;
        [printer appendText:[NSString stringWithFormat:@"服 务 员: %@",string6] alignment:HLTextAlignmentLeft] ;
        [printer appendSeperatorLineWithLeft] ;
        [printer appendNewLine] ;
        NSString *string7 = @"________________" ;
        [printer appendText:[NSString stringWithFormat:@"操作员签字: %@",string7] alignment:HLTextAlignmentLeft] ;
        for (int i = 0 ; i<6; i++) {
            [printer appendNewLine] ;
        }
        // 切纸
        [printer appendCutOff] ;
    }
    return printer;
}

#pragma mark -  消费明细单
- (HLPrinter *)consumption
{
    int count = 1 ;
    HLPrinter *printer = [[HLPrinter alloc] init];
    for (int i = 0 ; i<count; i++) {
        NSString *title = @"皇后酒吧";
        [printer appendText:title alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleBig];
        NSString *title2 = @"消费明细单";
        [printer appendText:title2 alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleMiddle];
        NSString *str1 = nil ;
        if(i==0)
        {
            str1 = [NSString stringWithFormat:@"［ 主 联 单 ］"] ;
        }
        else
        {
            str1 = [NSString stringWithFormat:@"［ 副 联 单 ］%d",i] ;
        }
        [printer appendText:str1 alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleSmalle];
        [printer appendSeperatorLineWithLeft];
        
        NSString *startString = @"1610250015792" ;
        [printer appendText:[NSString stringWithFormat:@"帐单编号: %@",startString] alignment:HLTextAlignmentLeft] ;
        NSString *endString = @"赵世杰" ;
        [printer appendText:[NSString stringWithFormat:@"交 款 人: %@",endString] alignment:HLTextAlignmentLeft] ;
        NSString *string1 = @"A10" ;
        [printer appendText:[NSString stringWithFormat:@"房台名称: %@",string1] alignment:HLTextAlignmentLeft] ;
        NSString *string2 = @"王江" ;
        [printer appendText:[NSString stringWithFormat:@"定 位 人: %@",string2] alignment:HLTextAlignmentLeft] ;
        [printer appendSeperatorLineWithLeft] ;
        
        [printer appendLeftText:@"商品" middleText:@"数量" rightText:@"单价" isTitle:YES];
        [printer appendLeftText:@"香槟王" middleText:@"1" rightText:@"888" isTitle:YES];
        [printer appendLeftText:@"香槟王" middleText:@"1" rightText:@"888" isTitle:YES];
        [printer appendLeftText:@"香槟王" middleText:@"1" rightText:@"888" isTitle:YES];
        [printer appendSeperatorLineWithLeft] ;
        
        
        NSString *string6 = @"888" ;
        [printer appendText:[NSString stringWithFormat:@"合集金额: %@",string6] alignment:HLTextAlignmentLeft] ;
        NSString *string7 = @"________________" ;
        [printer appendText:[NSString stringWithFormat:@"客户签字: %@",string7] alignment:HLTextAlignmentLeft] ;
        [printer appendSeperatorLineWithLeft] ;
        
        [printer appendText:@"付款方式" alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleSmalle];
        
        NSString *string8 = @"888" ;
        NSString *payWap8 = @"现金支付" ;
        [printer appendText:[NSString stringWithFormat:@"%@: %@",payWap8,string8] alignment:HLTextAlignmentLeft] ;
        NSString *string9 = @"888" ;
        NSString *payWap9 = @"微信支付" ;
        [printer appendText:[NSString stringWithFormat:@"%@: %@",payWap9,string9] alignment:HLTextAlignmentLeft] ;
        
        for (int i = 0 ; i<6; i++) {
            [printer appendNewLine] ;
        }
        // 切纸
        [printer appendCutOff] ;
    }
    return printer;

}

#pragma mark - 会员卡冲值小票
- (HLPrinter *)vipRecharge
{
    int count = 1 ;
    HLPrinter *printer = [[HLPrinter alloc] init];
    for (int i = 0 ; i<count; i++)
    {
        NSString *title = @"皇后酒吧";
        [printer appendText:title alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleBig];
        NSString *title2 = @"会员卡冲值";
        [printer appendText:title2 alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleMiddle];
        NSString *str1 = nil ;
        if(i==0)
        {
            str1 = [NSString stringWithFormat:@"［ 主 联 单 ］"] ;
        }
        else
        {
            str1 = [NSString stringWithFormat:@"［ 副 联 单 ］%d",i] ;
        }
        [printer appendText:str1 alignment:HLTextAlignmentCenter fontSize:HLFontSizeTitleSmalle];
        [printer appendSeperatorLineWithLeft];
        NSString *startString = @"123456789" ;
        [printer appendText:[NSString stringWithFormat:@"会员卡编号: %@",startString] alignment:HLTextAlignmentLeft] ;
        NSString *endString = @"赵世杰" ;
        [printer appendText:[NSString stringWithFormat:@"会员姓名  : %@",endString] alignment:HLTextAlignmentLeft] ;
        NSString *string1 = @"100" ;
        [printer appendText:[NSString stringWithFormat:@"冲值前余额: %@",string1] alignment:HLTextAlignmentLeft] ;
        NSString *string2 = @"10100" ;
        [printer appendText:[NSString stringWithFormat:@"冲值后余额: %@",string2] alignment:HLTextAlignmentLeft] ;
        NSString *string3 = @"101" ;
        [printer appendText:[NSString stringWithFormat:@"积      分: %@",string3] alignment:HLTextAlignmentLeft] ;
        [printer appendSeperatorLineWithLeft] ;
        
        NSString *string4 = @"李嘉浩" ;
        [printer appendText:[NSString stringWithFormat:@"收  银  员: %@",string4] alignment:HLTextAlignmentLeft] ;
        NSString *string5 = @"10000" ;
        [printer appendText:[NSString stringWithFormat:@"实收金额  : %@",string5] alignment:HLTextAlignmentLeft] ;
        NSString *string6 = @"0" ;
        [printer appendText:[NSString stringWithFormat:@"增值冲值额: %@",string6] alignment:HLTextAlignmentLeft] ;
        NSString *string7 = @"现金" ;
        [printer appendText:[NSString stringWithFormat:@"付款方式  : %@",string7] alignment:HLTextAlignmentLeft] ;
        NSString *string8 = @"周鹏旭" ;
        [printer appendText:[NSString stringWithFormat:@"提成人姓名: %@",string8] alignment:HLTextAlignmentLeft] ;
        
        for (int i = 0 ; i<6; i++) {
            [printer appendNewLine] ;
        }
        // 切纸
        [printer appendCutOff] ;
    }
    return printer;

}


@end
