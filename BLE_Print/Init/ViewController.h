//
//  ViewController.h
//  Init
//
//  Created by mc on 16/11/2.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic)   NSMutableArray              *deviceArray;  /**< 蓝牙设备个数 */


@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end

