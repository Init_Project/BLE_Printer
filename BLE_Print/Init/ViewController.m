//
//  ViewController.m
//  Init
//
//  Created by mc on 16/11/2.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import "ViewController.h"
#import "HLBLEManager.h"
#import "SVProgressHUD.h"
#import "BLEDetailViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.deviceArray = [NSMutableArray array] ;
    
    self.tableView.delegate = self ;
    self.tableView.dataSource = self ;
}

- (IBAction)searchClick:(id)sender {
    self.deviceArray = [[NSMutableArray alloc]init] ;
    self.tableView.rowHeight = 60 ;
    
    HLBLEManager *manager = [HLBLEManager sharedInstance] ;
    __weak HLBLEManager *weakManager = manager;
    manager.stateUpdateBlock = ^(CBCentralManager *central)
    {
        NSString *info = nil ;
        switch (central.state) {
            case CBManagerStatePoweredOn:
            {
                info = @"蓝牙已打开，并且可用";
                //三种种方式
                // 方式1
                [weakManager scanForPeripheralsWithServiceUUIDs:nil options:nil];
               break;
            }
            case CBManagerStatePoweredOff:
                info = @"蓝牙可用，未打开";
                break;
            case CBManagerStateUnsupported:
                info = @"SDK不支持";
                break;
            case CBManagerStateUnauthorized:
                info = @"程序未授权";
                break;
            case CBManagerStateResetting:
                info = @"CBCentralManagerStateResetting";
                break;
            case CBManagerStateUnknown:
                info = @"CBCentralManagerStateUnknown";
                break;
            default:
                break;
        }
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showInfoWithStatus:info ];
    };
    
    manager.discoverPeripheralBlcok = ^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
        if(peripheral.name.length<=0)
        {
            return ;
        }
        if(self.deviceArray.count == 0)
        {
            // RSSI 型号强度
            NSDictionary *dict = @{@"peripheral":peripheral,@"RSSI":RSSI} ;
            [self.deviceArray addObject:dict] ;
        }
        else
        {
            BOOL isExist = NO;
            for (int i = 0; i < self.deviceArray.count; i++) {
                NSDictionary *dict = [self.deviceArray objectAtIndex:i];
                CBPeripheral *per = dict[@"peripheral"];
                if ([per.identifier.UUIDString isEqualToString:peripheral.identifier.UUIDString]) {
                    isExist = YES;
                    NSDictionary *dict = @{@"peripheral":peripheral, @"RSSI":RSSI};
                    [_deviceArray replaceObjectAtIndex:i withObject:dict];
                }
            }
            if (!isExist) {
                NSDictionary *dict = @{@"peripheral":peripheral, @"RSSI":RSSI};
                [self.deviceArray addObject:dict];
            }
        }
        [self.tableView reloadData];
    } ;
   
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _deviceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"deviceId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    NSDictionary *dict = [self.deviceArray objectAtIndex:indexPath.row];
    CBPeripheral *peripherral = dict[@"peripheral"];
    cell.textLabel.text = [NSString stringWithFormat:@"名称:%@",peripherral.name];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"信号强度:%@",dict[@"RSSI"]];
    if (peripherral.state == CBPeripheralStateConnected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dict = [self.deviceArray objectAtIndex:indexPath.row];
    CBPeripheral *peripheral = dict[@"peripheral"];
    
    BLEDetailViewController *detailVC = [[BLEDetailViewController alloc]init];
    detailVC.perpheral = peripheral;
    UINavigationController *navig = [[UINavigationController alloc]initWithRootViewController:detailVC] ;
    [self presentViewController:navig animated:YES completion:nil] ;
}


@end
