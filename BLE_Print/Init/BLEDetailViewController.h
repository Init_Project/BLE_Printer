//
//  BLEDetailViewController.h
//  Init
//
//  Created by mc on 16/11/2.
//  Copyright © 2016年 zhaoshijie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BLEDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

// 选择连接的设备
@property (strong, nonatomic)CBPeripheral *perpheral;

@property (strong, nonatomic)   NSArray            *goodsArray;  /**< 商品数组 */

@property (strong, nonatomic)   NSMutableArray            *infos;  /**< 详情数组 */

@property (strong, nonatomic)   CBCharacteristic            *chatacter;  /**< 可写入数据的特性 */

@property (weak, nonatomic) IBOutlet UITableView *tableView;

// 存放标题的数组
@property(nonatomic,strong)NSArray *titleArray ;


@end
